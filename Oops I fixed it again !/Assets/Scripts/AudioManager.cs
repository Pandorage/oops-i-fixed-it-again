﻿using System;
using UnityEngine;
using UnityEngine.Audio; 
using DG.Tweening;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    
    public static AudioManager Instance;
    

    void Awake()
    {
        DontDestroyOnLoad(gameObject);  //On ne détruit pas l'objet pendant le chargement
        //On conserve l'objet même en changeant de scène
        if (Instance == null)
        {
            Instance = this;    //Si Instance n'est pas défini, on définit Instance avec notre objet, sinon, on le détruit
        }
        else
        {
            Destroy(gameObject);
        }


        foreach (Sound s in sounds) //Pour chaque son dans notre tableau, on va chercher sa source, son volume et l'option pour le loop
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;

            s.source.loop = s.loop;
        }
    }



    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name); //On joue le son nommé par le nom tapé
        s.source.Play();
        s.source.DOFade(0.7f, 10f);    //On met un fade pour l'arrivée du son pour plus de JUICE
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name); //On arrête le son nommé par le nom tapé
        s.source.DOFade(0f, 10f);    //On met un fade pour le départ du son pour plus de JUICE
        //s.source.Stop();
    }

    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name); //On joue le son nommé par le nom tapé
        s.source.Play();
    }
    
    public void StopSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name); //On arrête le son nommé par le nom tapé
        s.source.Stop();
    }
}