﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Baguette : MonoBehaviour
{
    public GameObject projectilePrefab;
    public BoolVariable baguetteEnabled;
    public BoolVariable useBaguette;

    void Update()
    {

        if (baguetteEnabled.value && useBaguette.value)
        {
            var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            if (Input.GetButtonDown("Fire1"))
            {
                Instantiate(projectilePrefab, transform.position, Quaternion.AngleAxis(angle, Vector3.forward));
            }
        }
    }
}
