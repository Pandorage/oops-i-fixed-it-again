﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    public AudioSource music;
    public float cooldown = 0.3f;
    public bool onCooldown = false;
    public BoolVariable onMove, aerial;
    // Start is called before the first frame update
    void Start()
    {
        music = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void Update()
    {
        if (onMove.value && !music.isPlaying && cooldown == 0.3f)
        {
            Footstep();
            onCooldown = true;
        }

        if (onCooldown)
        {
            cooldown -= Time.deltaTime;
                if (cooldown <= 0f)
                {
                    onCooldown = false;
                    cooldown = 0.3f;
                }
        }

        if (!onMove.value)
        {
            music.Stop();
        }
        
    }

    public void Footstep()
    {
        music.Play();
        
    }
}
