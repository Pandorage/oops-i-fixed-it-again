﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using DG.Tweening;

public class GameplayManager : MonoBehaviour
{
    public BoolVariable jump, doublejump, glide, baguette, plannar, freezePlayer, onMove, aerial;

    public IntVariable numPiste, morceauxBalais;
 
    public FloatVariable playerSpeed;
    public ColorCorrectionCurves colorCorrect;
    private int _actualColor;

    public static GameplayManager Instance;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;    //Si Instance n'est pas défini, on définit Instance avec notre objet, sinon, on le détruit
        }
        else
        {
            Destroy(gameObject);
        }
        
        jump.value = false;
        doublejump.value = false;
        glide.value = false;
        baguette.value = false;
        plannar.value = false;
        freezePlayer.value = false;
        onMove.value = false;
        aerial.value = false;
        
        morceauxBalais.value = 0;
        numPiste.value = 0;
        playerSpeed.value = 10;
        
        
    }

    public void Color()
    {
        if (_actualColor < 5)
        {
            _actualColor++;
            colorCorrect.saturation = _actualColor * 0.15f;
        }
    }
}
