﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NotesDeMusique : MonoBehaviour
{
    public BoolVariable bonus;

    private AudioManager _audioManager;
    public IntVariable numPiste;

    void Awake()
    {
        _audioManager = FindObjectOfType<AudioManager>();    //On va chercher le script AudioManager
        numPiste.value = 0;    //On définit la valeur de notre Scriptable Object à 0
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        //Si le player entre en contact avec la note de music, on passe le Scriptable object boolean des bonus sur True. On change la musique en train de se jouer, et on détruit la note de musique
        if (other.gameObject.CompareTag("Player"))
        {
            bonus.value = true;
            
            if (numPiste.value > 0)
            {
                
                _audioManager.Stop(numPiste.value.ToString());
            }

            if (numPiste.value <= 5)
            {
                numPiste.value++;
            }
            
            _audioManager.Play(numPiste.value.ToString());
            GameplayManager.Instance.Color();
            Destroy(gameObject);
        } 
    }
}
