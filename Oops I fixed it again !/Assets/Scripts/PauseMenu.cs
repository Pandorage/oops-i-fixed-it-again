﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu, player;
    public BoolVariable freezePlayer;

    private bool _isPaused = false;
    //public GameObject playerMovement;

    void Awake()
    {
        
    }
    
    void Update()
    {
        if (Input.GetButtonDown("Cancel") && !_isPaused)
        {
            _isPaused = true;
            freezePlayer.value = true;
            pauseMenu.SetActive(true);
        }
        else if (Input.GetButtonDown("Cancel") && _isPaused)
        {
            _isPaused = false;
            freezePlayer.value = false;
            pauseMenu.SetActive(false);
        }
    }

    public void Resume()
    {
        _isPaused = false;
        freezePlayer.value = false;
        pauseMenu.SetActive(false);
        
    }

    public void Quit()
    {
        Application.Quit();
    }
}
