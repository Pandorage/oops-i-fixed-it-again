﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateformeCassée : MonoBehaviour
{
    public Sprite fixedSrpite;
    public bool repared;
    void Start()
    {
        repared = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Projectile")
        {
            GetComponent<SpriteRenderer>().sprite = fixedSrpite;
            GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }
}
