﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public LayerMask groundLayer;

    public bool onGround;

    public float collisionRadius = 0.17f;
    public Vector2 bottomOffset;

    //Vérification des collisions au sol et au mur;
    void Update()
    {
        onGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
    }
    
    //Débug du la hitbox de détection de sol
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere((Vector2)transform.position  + bottomOffset, collisionRadius);
    }
}
