﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public AudioManager audioManager;
    private float _cooldown = 0.3f;
    private bool _isPlayingLevitate, _isPlayingFootstep;

    public BoolVariable onMove, aerial;

        [Header("Stats")]
    public FloatVariable speed; //10
    public float jumpForce = 15;

    [Header("Bonus")]
    public BoolVariable jump;
    public BoolVariable doubleJump;
    public BoolVariable glide;
    public BoolVariable useBaguette;
    public BoolVariable baguetteEnabled;

    private Rigidbody2D _rb;
    private PlayerCollision _coll;
    private BetterJumping _betJump;
    private SpriteRenderer _sr;
    private Animator _anim;
    private bool secondJump;
    
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _coll = GetComponent<PlayerCollision>();
        _betJump = GetComponent<BetterJumping>();
        _sr = GetComponent<SpriteRenderer>();
        _anim = GetComponent<Animator>();

        _isPlayingFootstep = false;
        _isPlayingLevitate = false;
    }

    void Update()
    {
        //Récupération des contrôles
        float x = Input.GetAxis("Horizontal");
        Vector2 dir = new Vector2(x, 0);
        _anim.SetFloat("Speed", dir.sqrMagnitude);
        _anim.SetFloat("Vertical", _rb.velocity.y);
        if (x > 0)
        {
            _sr.flipX = false;
        }
        else if(x < 0)
        {
            _sr.flipX = true;
        }

        if (_coll.onGround && baguetteEnabled.value && dir.x == 0)
        {
            useBaguette.value = true;
        }
        else
        {
            useBaguette.value = false;
        }

        if (baguetteEnabled.value)
        {
            _anim.SetBool("Baguette", useBaguette.value);
        }
        
        //Déplacement du personnage
        Walk(dir);

        //Gestion du saut
        if (Input.GetButtonDown("Jump") && jump.value)
        {
            if (_coll.onGround)
            {
                Jump();
                
            } else if (secondJump && doubleJump.value)
            {
                //Gestion du double saut
                Jump();
                secondJump = false;
            }
        }

        if (_rb.velocity.y <= 0 && glide.value && Input.GetButton("Jump"))
        {
            if (!_isPlayingLevitate)
            {
                PlaySoundLevitate();
                _isPlayingLevitate = true;
            }
            
            _betJump.gliding = true;
            _rb.gravityScale = 0.5f;
        }
        else
        {
            _isPlayingLevitate = false;
            audioManager.StopSound("Levitate");
            _betJump.gliding = false;
            _rb.gravityScale = 3;
        }

        //Reset du double saut et Bloquer le son des pas en l'air
        if (_coll.onGround)
        {
            secondJump = true;
            aerial.value = false;
        }
        else
        {
            aerial.value = true;
        }
        _anim.SetBool("Grounded", _coll.onGround);
    }

    //Fonction de déplacement horizontal
    private void Walk(Vector2 dir)
    {
        _rb.velocity = (new Vector2(dir.x * speed.value, _rb.velocity.y));

        if (_rb.velocity.x == 0)
        {
            onMove.value = false;
        }
        else
        {
            onMove.value = true;
        }
    }

    //Fonction de saut
    private void Jump()
    {
        audioManager.PlaySound("Jump");
        _rb.velocity = new Vector2(_rb.velocity.x, 0);
        _rb.velocity += Vector2.up * jumpForce;
    }

    void PlaySoundLevitate()
    {
        audioManager.PlaySound("Levitate");
    }
}
