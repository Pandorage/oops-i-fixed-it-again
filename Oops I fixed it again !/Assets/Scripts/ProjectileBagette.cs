﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBagette : MonoBehaviour
{
    
    public float speed = 20f;
    public Rigidbody2D rb;

    public Animator animator;
    

    void Start()
    {
        rb.velocity = transform.right * speed;

    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Ground")
        {
            animator.SetBool("contact",true);
            rb.velocity = transform.right * speed/3;
            Destroy (gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length/2); 
        }
        else if (other.gameObject.tag == "Plateforme")
        {
            animator.SetBool("contact",true);
            rb.velocity = transform.right * speed/3;
            Destroy (gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length/2); 
        }
    }
    
}
