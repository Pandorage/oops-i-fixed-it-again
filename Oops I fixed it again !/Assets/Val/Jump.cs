﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public float jumpForce;
    public Rigidbody2D rb;
    public bool isGrounded = false;
    public Transform groundCheck;
    public LayerMask whatIsGround;
    public float checkRadius;
    // Start is called before the first frame update
    void Start()
    {
    }
    public void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) && isGrounded)
        {
            rb.velocity = Vector2.up * jumpForce;
        }
    }

    
}
