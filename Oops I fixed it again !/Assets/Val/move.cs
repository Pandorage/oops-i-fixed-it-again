﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class move : MonoBehaviour
{
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void FixedUpdate()
    {
      
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }

       
    }
    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Equals("Cloud"))
        {
            transform.parent = col.transform;
        }

        if (col.gameObject.CompareTag("Note1"))
            {
                Destroy(col.gameObject);
                gameObject.GetComponent<Jump>().enabled = true;
            }
    }
    public void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.name.Equals("Cloud"))
            transform.parent = null;
    }
}
